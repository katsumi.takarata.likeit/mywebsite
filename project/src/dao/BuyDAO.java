package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import base.DBManager;
import beans.BuyDataBeans;

public class BuyDAO {
	//購入情報登録
	public BuyDataBeans insertBuy(int userId, int totalPrice, int deliveryMethodId, int ticketId, int ticketBuyNumber) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getConnection();
			ps = conn.prepareStatement(
					"INSERT INTO t_buy(user_id,total_price,delivery_method_id,ticket_id,create_date,up_date,ticket_buy_number) VALUES(?,?,?,?,NOW(),NOW(),?)");
			ps.setInt(1, userId);
			ps.setInt(2, totalPrice);
			ps.setInt(3, deliveryMethodId);
			ps.setInt(4, ticketId);
			ps.setInt(5, ticketBuyNumber);
			ps.executeUpdate();

			System.out.println("insertBuyDAO completed");

			return null;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	//購入履歴検索
	public ArrayList<BuyDataBeans> getBuyHistory(int userId) {
		Connection conn = null;
		PreparedStatement ps = null;
		ArrayList<BuyDataBeans> buyList = new ArrayList<BuyDataBeans>();
		try {
			conn = DBManager.getConnection();

			//確認済みのSQL
			ps = conn.prepareStatement(
					"SELECT * FROM t_buy WHERE user_id = ? ORDER BY create_date");
			//SQL実行
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();
			// 失敗時の処理 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// ログイン成功時の処理 必要なデータのみインスタンスのフィールドに追加
			while (rs.next()) {
				int idd = rs.getInt("id");
				int userId2 = rs.getInt("user_id");
				int totalPrice  = rs.getInt("total_price");
				int deliveryMethodId = rs.getInt("delivery_method_id");
				int tikcetId = rs.getInt("ticket_id");
				Date createDate = rs.getDate("create_date");
				Date upDate = rs.getDate("up_date");
				int ticketBuyNumber = rs.getInt("ticket_buy_number");
				BuyDataBeans buy = new BuyDataBeans(idd,userId2,totalPrice,deliveryMethodId,tikcetId,createDate,upDate,ticketBuyNumber);
				buyList.add(buy);
			}
			System.out.println("getBuyHistoryDAO completed");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return buyList;
	}
	//ユーザーIDから購入履歴詳細検索
		public BuyDataBeans getBuyHistoryDetail(int userId) {
			Connection conn = null;
			PreparedStatement ps = null;
			try {
				conn = DBManager.getConnection();

				//確認済みのSQL
				ps = conn.prepareStatement(
						"SELECT * FROM t_buy WHERE user_id = ?");
				//SQL実行
				ps.setInt(1, userId);
				ResultSet rs = ps.executeQuery();

					int id = rs.getInt("id");
					int userId2 = rs.getInt("user_id");
					int totalPrice  = rs.getInt("total_price");
					int deliveryMethodId = rs.getInt("delivery_method_id");
					int tikcetId = rs.getInt("ticket_id");
					Date createDate = rs.getDate("create_date");
					Date upDate = rs.getDate("up_date");
					int ticketBuyNumber = rs.getInt("ticket_buy_number");

					System.out.println("getBuyHistoryDetailDAO completed");

					return new BuyDataBeans(id, userId2, totalPrice, deliveryMethodId, tikcetId, createDate, upDate,ticketBuyNumber);
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}

		//チケットIDから購入履歴詳細検索
		public BuyDataBeans getBuyHistoryDetailByItemId(int itemId) {
			Connection conn = null;
			PreparedStatement ps = null;
			try {
				conn = DBManager.getConnection();

				//確認済みのSQL
				ps = conn.prepareStatement(
						"SELECT * FROM t_buy WHERE ticket_id = ?");
				//SQL実行
				ps.setInt(1, itemId);
				ResultSet rs = ps.executeQuery();

				// 失敗時の処理 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
				if (!rs.next()) {
					return null;
				}

					int id = rs.getInt("id");
					int userId2 = rs.getInt("user_id");
					int totalPrice  = rs.getInt("total_price");
					int deliveryMethodId = rs.getInt("delivery_method_id");
					int ticketId = rs.getInt("ticket_id");
					Date createDate = rs.getDate("create_date");
					Date upDate = rs.getDate("up_date");
					int ticketBuyNumber = rs.getInt("ticket_buy_number");

					System.out.println("getBuyHistoryDetailByItemIdDAO completed");

					return new BuyDataBeans(id, userId2, totalPrice, deliveryMethodId, ticketId, createDate, upDate,ticketBuyNumber);
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}
	//購入履歴検索
//	public BuyDataBeans getBuyHistory(int userId) {
//		Connection conn = null;
//		PreparedStatement ps = null;
//		try {
//			conn = DBManager.getConnection();
//			ps = conn.prepareStatement(
//					"SELECT tt.performance, tt.date , tt.artist , tt.place , tt.number , tt.price ,tt.seat ,tt.explanation , tt.end_date ,tb.total_price , tb.create_date , m.name ,m.price FROM t_buy tb INNER JOIN t_ticket tt INNER JOIN m_delivery_method m ON tb.ticket_id = tt.id WHERE tb.user_id = ? AND m.id = tb.delivery_method_id ");
//			ps.setInt(1, userId);
//			ResultSet rs = ps.executeQuery();
//			while (rs.next()) {
//
//			}
//
//			System.out.println("getBuyHistoryDAO completed");
//
//			return null;
//		} catch (SQLException e) {
//			e.printStackTrace();
//			return null;
//		} finally {
//			// データベース切断
//			if (conn != null) {
//				try {
//					conn.close();
//				} catch (SQLException e) {
//					e.printStackTrace();
//					return null;
//				}
//			}
//		}
//	}
}
