package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserDataBeans;

public class UserDAO {
	//ログイン時の処理
	public static UserDataBeans findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {

			// データベースへ接続
			conn = DBManager.getConnection();
			//暗号化
			String pass = md5(password);

			//SQL内容
			String sql = "SELECT * FROM t_user WHERE login_id = ? and password = ?";
			// SELECTを実行し、結果表を取得
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, pass);
			ResultSet rs = ps.executeQuery();

			// ログイン失敗時の処理 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// ログイン成功時の処理 必要なデータのみインスタンスのフィールドに追加
			int idData = rs.getInt("id");
			String nameData = rs.getString("user_name");
			return new UserDataBeans(idData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	//新規登録の処理
	public static void create(String userName, String address, String loginId, String password, String birthDate) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getConnection();
			//暗号化
			String pass = md5(password);
			//確認済みのSQL
			ps = conn.prepareStatement(
					"INSERT INTO t_user (user_name,address,login_id,password,birth_date,create_date,up_date)VALUES(?,?,?,?,?,NOW(),NOW())");
			//INSERT実行
			ps.setString(1, userName);
			ps.setString(2, address);
			ps.setString(3, loginId);
			ps.setString(4, pass);
			ps.setString(5, birthDate);
			ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//ログインIDの重複を探すメソッド
	public static boolean findLoginId(String loginId) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			//確認済みのSQL
			String sql = ("SELECT * FROM t_user WHERE login_id = ?");
			// SELECTを実行し、結果表を取得
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, loginId);
			ResultSet rs = ps.executeQuery();

			// 既存のユーザIDが見つからなかった時の処理
			if (!rs.next()) {
				return false;
			}
			// 既存のユーザIDが見つかった時の処理
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return false;
				}
			}
		}
	}

	//URLパラメータで送られたIDでユーザー詳細情報を出す
	public UserDataBeans userDetail(int id) {
		Connection conn = null;
		UserDataBeans detail = null;
		try {
			//データベースへ接続
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM t_user WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int ID = rs.getInt("id");
				String name = rs.getString("user_name");
				String address = rs.getString("address");
				String loginId = rs.getString("login_id");
				Date birthDate = rs.getDate("birth_date");
				String createDate = rs.getString("create_date");
				String upDate = rs.getString("up_date");
				detail = new UserDataBeans(ID,name,address,loginId,birthDate,createDate,upDate);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e2) {
					e2.printStackTrace();
					return null;
				}
			}

		}
		return detail;

	}
	//ユーザー情報更新
	public void update(int userId, String password, String address, String name,String birth) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getConnection();
			//暗号化
			String pass = md5(password);
			//確認済みのSQL
			ps = conn.prepareStatement(
					"UPDATE t_user SET user_name = ?,address = ?,password = ?,birth_date = ?,up_date = NOW() WHERE id = ?");
			//実行
			ps.setString(1, name);
			ps.setString(2, address);
			ps.setString(3, pass);
			ps.setString(4, birth);
			ps.setInt(5, userId);
			ps.executeUpdate();
			System.out.println("updateDAO completed");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	//パスワードとパスワード(確認)がどちらも空欄の場合はパスワードは更新せず、パスワード以外の項目を更新する
	public void updateNotPass(int userId, String address, String name, String birth) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getConnection();

			//確認済みのSQL
			ps = conn.prepareStatement(
					"UPDATE t_user SET user_name = ?,address = ?,birth_date = ?,up_date = NOW() WHERE id = ?");
			//実行
			ps.setString(1, name);
			ps.setString(2, address);
			ps.setString(3, birth);
			ps.setInt(4, userId);
			ps.executeUpdate();
			System.out.println("updateNotPass completed");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	//ユーザー消去のメソッド
	public UserDataBeans Delete(int id) {
		Connection conn = null;
		try {
			//データベースへ接続
			conn = DBManager.getConnection();
			String sql = "DELETE FROM t_user WHERE id = ? LIMIT 1";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e2) {
					e2.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}
	//暗号化のメソッド
	private static String md5(String password) {
		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力 passwordがresultに変わりました
		System.out.println(result);
		return result;
	}


}
