package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.CategoryMethodDataBeans;

public class CategoryMethodDAO {
	//DBに登録されているカテゴリを取得
	public ArrayList<CategoryMethodDataBeans> getAllCategoryMethodDataBeans(){
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getConnection();
			ps = conn.prepareStatement("SELECT * FROM m_category_method");
			ResultSet rs = ps.executeQuery();

			ArrayList<CategoryMethodDataBeans> categoryMethodDataBeansList = new ArrayList<CategoryMethodDataBeans>();
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				CategoryMethodDataBeans list = new CategoryMethodDataBeans(id,name);
				categoryMethodDataBeansList.add(list);
			}

			System.out.println("getAllCategoryMethodDataBeansDAO completed");

			return categoryMethodDataBeansList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	//カテゴリをIDをもとに取得
		public CategoryMethodDataBeans getCategoryMethodDataBeansByID(int CategoryMethodId) {
			Connection conn = null;
			PreparedStatement ps = null;
			try {
				conn = DBManager.getConnection();
				ps = conn.prepareStatement("SELECT * FROM m_category_method WHERE id = ?");
				ps.setInt(1, CategoryMethodId);
				ResultSet rs = ps.executeQuery();
				CategoryMethodDataBeans cMDB = new CategoryMethodDataBeans();
				while (rs.next()) {
					cMDB.setId(rs.getInt("id"));
					cMDB.setName(rs.getString("name"));
				}
				System.out.println("getDeliveryMethodDataBeansByIDDAP comleted");
				return cMDB;
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}
}
