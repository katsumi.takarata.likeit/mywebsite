package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.DeliveryMethodDataBeans;


public class DeliveryMethodDAO {
	//DBに登録されている配送方法を取得
	public ArrayList<DeliveryMethodDataBeans> getAllDeliveryMethodDataBeans(){
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getConnection();
			ps = conn.prepareStatement("SELECT * FROM m_delivery_method");
			ResultSet rs = ps.executeQuery();

			ArrayList<DeliveryMethodDataBeans> deliveryMethodDataBeansList = new ArrayList<DeliveryMethodDataBeans>();
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int price = rs.getInt("price");
				DeliveryMethodDataBeans list = new DeliveryMethodDataBeans(id,name,price);
				deliveryMethodDataBeansList.add(list);
			}

			System.out.println("searching all DeliveryMethodDataBeans has been completed");

			return deliveryMethodDataBeansList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}
	//配送方法をIDをもとに取得
	public DeliveryMethodDataBeans getDeliveryMethodDataBeansByID(int DeliveryMethodId) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getConnection();
			ps = conn.prepareStatement("SELECT * FROM m_delivery_method WHERE id = ?");
			ps.setInt(1, DeliveryMethodId);
			ResultSet rs = ps.executeQuery();
			DeliveryMethodDataBeans dmdb = new DeliveryMethodDataBeans();
			while (rs.next()) {
				dmdb.setId(rs.getInt("id"));
				dmdb.setName(rs.getString("name"));
				dmdb.setPrice(rs.getInt("price"));
			}
			return dmdb;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}