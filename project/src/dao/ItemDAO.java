package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import base.DBManager;
import beans.ItemDataBeans;

public class ItemDAO {
	//商品検索
	public ArrayList<ItemDataBeans> search(String seachword){
		Connection conn = null;
		PreparedStatement ps = null;
		ArrayList<ItemDataBeans> seachList = new ArrayList<ItemDataBeans>();
		try {
			conn = DBManager.getConnection();

			//確認済みのSQL
			ps = conn.prepareStatement(
					"SELECT * FROM t_ticket WHERE (performance like ? or artist like ?) and sale_flag = 0");
			//SQL実行
			ps.setString(1, '%' + seachword + '%');
			ps.setString(2, '%' + seachword + '%');
			ResultSet rs = ps.executeQuery();


			// 成功時の処理 必要なデータのみインスタンスのフィールドに追加
			while (rs.next()) {
				int ID = rs.getInt("id");
				String performance = rs.getString("performance");
				Date date = rs.getDate("date");
				String artist = rs.getString("artist");
				String place = rs.getString("place");
				int number = rs.getInt("number");
				int price = rs.getInt("price");
				String seat = rs.getString("seat");
				String explanation = rs.getString("explanation");
				Date endDate = rs.getDate("end_date");
				int saleFlag = rs.getInt("sale_flag");
				int userId = rs.getInt("user_id");
				Date createDate = rs.getDate("create_date");
				Date upDate = rs.getDate("up_date");
				int categoryId = rs.getInt("category_id");
				System.out.println("ItemDetailDAO completed");
				ItemDataBeans seach = new ItemDataBeans(ID, performance, date, artist, place, number, price, seat, explanation, endDate,
						saleFlag, userId, createDate, upDate, categoryId);
				seachList.add(seach);
			}
			System.out.println("searchDAO completed");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return seachList;
	}


	//カテゴリ検索
	public ArrayList<ItemDataBeans> categorySeach(int categoryId) {
		Connection conn = null;
		PreparedStatement ps = null;
		ArrayList<ItemDataBeans> categorySeachList = new ArrayList<ItemDataBeans>();
		try {
			conn = DBManager.getConnection();

			//確認済みのSQL
			ps = conn.prepareStatement(
					"SELECT * FROM t_ticket WHERE category_id = ? and sale_flag = 0");
			//SQL実行
			ps.setInt(1, categoryId);
			ResultSet rs = ps.executeQuery();


			// 成功時の処理 必要なデータのみインスタンスのフィールドに追加
			while (rs.next()) {
				int ID = rs.getInt("id");
				String performance = rs.getString("performance");
				Date date = rs.getDate("date");
				String artist = rs.getString("artist");
				String place = rs.getString("place");
				int number = rs.getInt("number");
				int price = rs.getInt("price");
				String seat = rs.getString("seat");
				String explanation = rs.getString("explanation");
				Date endDate = rs.getDate("end_date");
				int saleFlag = rs.getInt("sale_flag");
				int userId = rs.getInt("user_id");
				Date createDate = rs.getDate("create_date");
				Date upDate = rs.getDate("up_date");
				int categoryId2 = rs.getInt("category_id");
				System.out.println("ItemDetailDAO completed");
				ItemDataBeans categorySeach = new ItemDataBeans(ID, performance, date, artist, place, number, price, seat, explanation, endDate,
						saleFlag, userId, createDate, upDate, categoryId2);
				categorySeachList.add(categorySeach);
			}
			System.out.println("categorySeachDAO completed");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return categorySeachList;
	}

	//出品登録
	public void putup(String performance, String date, String artist, String place, String number, String price,
			String seat, String explanation, String endDate, int saleFlag, int userId, String categoryId) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getConnection();

			//確認済みのSQL
			ps = conn.prepareStatement(
					"INSERT INTO t_ticket (performance,date,artist,place,number,price,seat,explanation,end_date,sale_flag,user_id,create_date,up_date,category_id)VALUES(?,?,?,?,?,?,?,?,?,?,?,NOW(),NOW(),?)");
			//INSERT実行
			ps.setString(1, performance);
			ps.setString(2, date);
			ps.setString(3, artist);
			ps.setString(4, place);
			ps.setString(5, number);
			ps.setString(6, price);
			ps.setString(7, seat);
			ps.setString(8, explanation);
			ps.setString(9, endDate);
			ps.setInt(10, saleFlag);
			ps.setInt(11, userId);
			ps.setString(12, categoryId);
			ps.executeUpdate();
			//完了のサイン
			System.out.println("putupDAO completed");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//チケット詳細
	public ItemDataBeans ItemDetail(int id) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getConnection();

			//確認済みのSQL
			ps = conn.prepareStatement(
					"SELECT * FROM t_ticket WHERE id = ?");
			//SQL実行
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			// 失敗時の処理 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// ログイン成功時の処理 必要なデータのみインスタンスのフィールドに追加
			int ID = rs.getInt("id");
			String performance = rs.getString("performance");
			Date date = rs.getDate("date");
			String artist = rs.getString("artist");
			String place = rs.getString("place");
			int number = rs.getInt("number");
			int price = rs.getInt("price");
			String seat = rs.getString("seat");
			String explanation = rs.getString("explanation");
			Date endDate = rs.getDate("end_date");
			int saleFlag = rs.getInt("sale_flag");
			int userId = rs.getInt("user_id");
			Date createDate = rs.getDate("create_date");
			Date upDate = rs.getDate("up_date");
			int categoryId = rs.getInt("category_id");
			System.out.println("ItemDetailDAO completed");
			return new ItemDataBeans(ID, performance, date, artist, place, number, price, seat, explanation, endDate,
					saleFlag, userId, createDate, upDate, categoryId);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//出品履歴
	public ArrayList<ItemDataBeans> putUpHistoryList(int userId) {
		Connection conn = null;
		PreparedStatement ps = null;
		ArrayList<ItemDataBeans> list = new ArrayList<ItemDataBeans>();
		try {
			conn = DBManager.getConnection();

			//確認済みのSQL
			ps = conn.prepareStatement(
					"SELECT * FROM t_ticket WHERE user_id = ? ORDER BY create_date DESC ");
			//SQL実行
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();
			// 結果表に格納されたレコードの内容を
			// ItemDataBeansインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String performance = rs.getString("performance");
				Date date = rs.getDate("date");
				String artist = rs.getString("artist");
				String place = rs.getString("place");
				int number = rs.getInt("number");
				int price = rs.getInt("price");
				String seat = rs.getString("seat");
				String explanation = rs.getString("explanation");
				Date endDate = rs.getDate("end_date");
				int saleFlag = rs.getInt("sale_flag");
				int userid = rs.getInt("user_id");
				Date createDate = rs.getDate("create_date");
				Date upDate = rs.getDate("up_date");
				int categoryId = rs.getInt("category_id");

				ItemDataBeans putUpHistory = new ItemDataBeans(id, performance, date, artist, place, number, price,
						seat, explanation, endDate, saleFlag, userid, createDate, upDate, categoryId);
				list.add(putUpHistory);
			}
			//完了のサイン
			System.out.println("putUpHistoryListDAO completed");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	//出品編集
	public void putUpUpdate(int itemId,String performance,String date,String artist,String place,String number,String price,String seat,String explanation,String endDate,String categoryId) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getConnection();

			//確認済みのSQL
			ps = conn.prepareStatement(
					"UPDATE t_ticket SET performance = ?,date = ?,artist = ?,place = ?,number = ?,price = ?,seat = ?,explanation = ?,end_date = ?,up_date = NOW(),category_id = ? WHERE id = ?");
			//SQL実行
			ps.setString(1, performance);
			ps.setString(2, date);
			ps.setString(3, artist);
			ps.setString(4, place);
			ps.setString(5, number);
			ps.setString(6, price);
			ps.setString(7, seat);
			ps.setString(8, explanation);
			ps.setString(9, endDate);
			ps.setString(10, categoryId);
			ps.setInt(11, itemId);
			ps.executeUpdate();
			System.out.println("putUpUpdateDAO completed");

		} catch (Exception e) {
			e.printStackTrace();
			return;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}
		}
	}
	//出品削除
	public void putUpDelete(int itemId) {

		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getConnection();

			//確認済みのSQL
			ps = conn.prepareStatement(
					"DELETE FROM t_ticket WHERE id = ?");
			//SQL実行
			ps.setInt(1, itemId);
			ps.executeUpdate();
			System.out.println("putUpDeleteDAO completed");

		} catch (Exception e) {
			e.printStackTrace();
			return;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}
		}
	}

	//ランダムに出品された商品を4個表示させる
	public ArrayList<ItemDataBeans> getRandItem(int s) {
		Connection conn = null;
		PreparedStatement ps = null;
		ArrayList<ItemDataBeans> list = new ArrayList<ItemDataBeans>();
		try {
			conn = DBManager.getConnection();

			//確認済みのSQL sale_flag = 0とはまだ売れていない物
			ps = conn.prepareStatement(
					"SELECT * FROM t_ticket WHERE sale_flag = 0 ORDER BY RAND() LIMIT ? ");
			//SQL実行
			ps.setInt(1, s);
			ResultSet rs = ps.executeQuery();
			// 結果表に格納されたレコードの内容を
			// ItemDataBeansインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String performance = rs.getString("performance");
				Date date = rs.getDate("date");
				String artist = rs.getString("artist");
				String place = rs.getString("place");
				int number = rs.getInt("number");
				int price = rs.getInt("price");
				String seat = rs.getString("seat");
				String explanation = rs.getString("explanation");
				Date endDate = rs.getDate("end_date");
				int saleFlag = rs.getInt("sale_flag");
				int userid = rs.getInt("user_id");
				Date createDate = rs.getDate("create_date");
				Date upDate = rs.getDate("up_date");
				int categoryId = rs.getInt("category_id");

				ItemDataBeans putUpHistory = new ItemDataBeans(id, performance, date, artist, place, number, price,
						seat, explanation, endDate, saleFlag, userid, createDate, upDate, categoryId);
				list.add(putUpHistory);
			}
			//完了のサイン
			System.out.println("getRandItemDAO completed");
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}

		}
		return list;

	}

	//最近出品された商品を4個表示させる
	public ArrayList<ItemDataBeans> RecentlyPutUpItem(int s) {
		Connection conn = null;
		PreparedStatement ps = null;
		ArrayList<ItemDataBeans> list = new ArrayList<ItemDataBeans>();
		try {
			conn = DBManager.getConnection();

			//確認済みのSQL sale_flag = 0とはまだ売れていない物
			ps = conn.prepareStatement(
					"SELECT * FROM t_ticket WHERE sale_flag = 0 ORDER BY create_date DESC LIMIT ?");
			//SQL実行
			ps.setInt(1, s);
			ResultSet rs = ps.executeQuery();
			// 結果表に格納されたレコードの内容を
			// ItemDataBeansインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String performance = rs.getString("performance");
				Date date = rs.getDate("date");
				String artist = rs.getString("artist");
				String place = rs.getString("place");
				int number = rs.getInt("number");
				int price = rs.getInt("price");
				String seat = rs.getString("seat");
				String explanation = rs.getString("explanation");
				Date endDate = rs.getDate("end_date");
				int saleFlag = rs.getInt("sale_flag");
				int userid = rs.getInt("user_id");
				Date createDate = rs.getDate("create_date");
				Date upDate = rs.getDate("up_date");
				int categoryId = rs.getInt("category_id");

				ItemDataBeans putUpHistory = new ItemDataBeans(id, performance, date, artist, place, number, price,
						seat, explanation, endDate, saleFlag, userid, createDate, upDate, categoryId);
				list.add(putUpHistory);
			}
			//完了のサイン
			System.out.println("RecentlyPutUpItemDAO completed");
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	//ticketが売れた確認用にsaleFlagに1を足す
	public void saleFlagCount(int id,int one) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getConnection();

			//確認済みのSQL
			ps = conn.prepareStatement(
					"UPDATE t_ticket SET sale_flag = ? WHERE id = ? ");
			//INSERT実行
			ps.setInt(1, one);
			ps.setInt(2, id);
			ps.executeUpdate();
			//完了のサイン
			System.out.println("saleFlagDAO completed");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
