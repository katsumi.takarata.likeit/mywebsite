package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.BuyDataBeans;
import beans.CategoryMethodDataBeans;
import beans.ItemDataBeans;
import dao.BuyDAO;
import dao.CategoryMethodDAO;
import dao.ItemDAO;

/**
 * Servlet implementation class PutUpHistoryDetail
 */
@WebServlet("/PutUpHistoryDetail")
public class PutUpHistoryDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PutUpHistoryDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//パラメータを取得
		String itemidd = request.getParameter("id");
		int itemId = Integer.parseInt(itemidd);

		//idを引数にしてDAO実行し、スコープにセット
		ItemDAO itemDAO = new ItemDAO();
		ItemDataBeans itemDetail = itemDAO.ItemDetail(itemId);
		request.setAttribute("itemDetail", itemDetail);

		//購入されたチケット枚数を取得しスコープにセット
		BuyDAO buyDAO = new BuyDAO();
		BuyDataBeans ticketBuy = buyDAO.getBuyHistoryDetailByItemId(itemId);
		if (ticketBuy != null) {
			int ticketBuyNumber = ticketBuy.getTicketBuyNumber();
			request.setAttribute("ticketBuyNumber", ticketBuyNumber);
		}else {
			int itemNumber = itemDetail.getNumber();
			request.setAttribute("itemNumber", itemNumber);
		}









		//カテゴリidを取得
		CategoryMethodDAO categoryMethodDAO = new CategoryMethodDAO();
		int cateid = itemDetail.getCategoryId();

		//カテゴリidを引数にして配送方法を取得しスコープにセット
		CategoryMethodDataBeans categoryBeans = categoryMethodDAO.getCategoryMethodDataBeansByID(cateid);
		request.setAttribute("categoryBeans", categoryBeans);

		//jspへフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/putUpHistoryDetail.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
