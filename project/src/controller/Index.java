package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class Index
 */
@WebServlet("/Index")
public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Index() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
			//商品情報を取得
			ItemDAO itemDAO = new ItemDAO();
			//ランダムの4個の商品を取得し、リクエストスコープにセット
			ArrayList<ItemDataBeans> randomItemList = itemDAO.getRandItem(4);
			request.setAttribute("randomItemList", randomItemList);

			//最近出品された4個の商品を取得し、リクエストスコープにセットしフォワード
			ArrayList<ItemDataBeans> recentlyItemList = itemDAO.RecentlyPutUpItem(4);
			request.setAttribute("recentlyItemList", recentlyItemList);

			request.getRequestDispatcher("/WEB-INF/jsp/index.jsp").forward(request, response);

	}
}
