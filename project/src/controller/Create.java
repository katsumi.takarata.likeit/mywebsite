package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDAO;

/**
 * Servlet implementation class Create
 */
@WebServlet("/Create")
public class Create extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Create() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//パラメータの取得
		String address = request.getParameter("address");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String repassword = request.getParameter("repassword");
		String name = request.getParameter("name");
		String birth = request.getParameter("birth");

		UserDAO userDao = new UserDAO();

		//一つでも空欄があるとき
		if (address.equals("")||loginId.equals("")||password.equals("")||name.equals("")||birth.equals("")) {
			//スコープにエラ〜メッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
			dispatcher.forward(request,response);
			return;
		}
		//パスワードと確認用パスワードが異なるとき
		if (!password.equals(repassword)) {
			//スコープにエラ〜メッセージをセット
			request.setAttribute("errMsg1", "入力された内容は正しくありません");
			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
			dispatcher.forward(request,response);
			return;
		}
		//既に登録されてるログインIDが入力された時
		if (userDao.findLoginId(loginId)) {
			//スコープにエラ〜メッセージをセット
			request.setAttribute("errMsg2", "入力された内容は正しくありません");
			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
			dispatcher.forward(request,response);
			return;
		}
		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		userDao.create(name, address, loginId, password, birth);
		//ホーム画面へリダイレクト
		response.sendRedirect("Index");
	}

}
