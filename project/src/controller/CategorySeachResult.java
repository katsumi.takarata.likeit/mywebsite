package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class CategorySeachResult
 */
@WebServlet("/CategorySeachResult")
public class CategorySeachResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CategorySeachResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//カテゴリのパラメータを取得
		String categoryidd = request.getParameter("categoryId");
		int categoryId = Integer.parseInt(categoryidd);

		//パラメータを引数にしてDAOの検索メソッドを実行
		ItemDAO itemDAO = new ItemDAO();
		ArrayList<ItemDataBeans> categorySeachResultList = itemDAO.categorySeach(categoryId);

		//スコープにセットしてフォワード
		request.setAttribute("categorySeachResultList", categorySeachResultList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/categorySeachResult.jsp");
		dispatcher.forward(request, response);
	}

}
