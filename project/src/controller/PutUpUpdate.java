package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CategoryMethodDataBeans;
import beans.ItemDataBeans;
import dao.CategoryMethodDAO;
import dao.ItemDAO;

/**
 * Servlet implementation class PutUpUpdate
 */
@WebServlet("/PutUpUpdate")
public class PutUpUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PutUpUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//itemidを引数にしてDAOのメソッドを実行
		String itemId = request.getParameter("id");
		int itemid = Integer.parseInt(itemId);
		//itemIdを出力
		System.out.println("itemid = " + itemid);

		ItemDAO itemDAO = new ItemDAO();
		ItemDataBeans item = itemDAO.ItemDetail(itemid);

		//スコープにセットしフォワード
		request.setAttribute("item", item);
		//DBからカテゴリのリストを取得
				CategoryMethodDAO categoryMethodDAO = new CategoryMethodDAO();
				ArrayList<CategoryMethodDataBeans> categoryList = categoryMethodDAO.getAllCategoryMethodDataBeans();
				//スコープにセット
				request.setAttribute("categoryList", categoryList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/putUpUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//パラメータを取得
		String performance = request.getParameter("performance");
		String artist = request.getParameter("artist");
		String date = request.getParameter("date");
		String place = request.getParameter("place");
		String number = request.getParameter("number");
		String price = request.getParameter("price");
		String seat = request.getParameter("seat");
		String categoryId = request.getParameter("categoryId");
		String explanation = request.getParameter("explanation");
		String endDate = request.getParameter("endDate");
		int itemId = Integer.parseInt(request.getParameter("itemId"));
		//DAOのメソッドを実行
		ItemDAO itemDAO = new ItemDAO();
		itemDAO.putUpUpdate(itemId, performance, date, artist, place, number, price, seat, explanation, endDate, categoryId);
		//リダイレクト
		response.sendRedirect("PutUpHistory");
	}

}
