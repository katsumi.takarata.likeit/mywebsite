package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class PutUpDelete
 */
@WebServlet("/PutUpDelete")
public class PutUpDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PutUpDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//チケットIDを取得
		int itemId = Integer.parseInt(request.getParameter("id"));
		//DAOに引数を渡してスコープにセット
		ItemDAO itemDAO = new ItemDAO();
		ItemDataBeans itemDetail = itemDAO.ItemDetail(itemId);
		request.setAttribute("itemDetail", itemDetail);
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/putUpDelete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//チケットIDを取得
		String i = request.getParameter("id");
		int itemId = Integer.parseInt(request.getParameter("id"));
		System.out.println(itemId);
		//DAOに引数を渡してメソッドを実行
		ItemDAO itemDAO = new ItemDAO();
		itemDAO.putUpDelete(itemId);
		//リダイレクト
		response.sendRedirect("PutUpHistory");
	}

}
