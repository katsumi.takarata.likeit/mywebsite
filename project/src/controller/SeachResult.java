package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class SeachResult
 */
@WebServlet("/SeachResult")
public class SeachResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SeachResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//検索ワードのパラメータを取得
		String seachword = request.getParameter("seachword");
		//パラメータを引数にしてDAOの検索メソッドを実行
		ItemDAO itemDAO = new ItemDAO();
		ArrayList<ItemDataBeans> seachResultList = itemDAO.search(seachword);
		//スコープにセットしてフォワード
		request.setAttribute("seachResultList", seachResultList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/seachResult.jsp");
		dispatcher.forward(request, response);
	}

}
