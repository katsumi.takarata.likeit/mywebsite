package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CategoryMethodDataBeans;
import beans.UserDataBeans;
import dao.CategoryMethodDAO;
import dao.ItemDAO;

/**
 * Servlet implementation class PutUp
 */
@WebServlet("/PutUp")
public class PutUp extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PutUp() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//DBからカテゴリのリストを取得
		CategoryMethodDAO categoryMethodDAO = new CategoryMethodDAO();
		ArrayList<CategoryMethodDataBeans> categoryList = categoryMethodDAO.getAllCategoryMethodDataBeans();
		//スコープにセット
		request.setAttribute("categoryList", categoryList);
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/putUp.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		 HttpSession session = request.getSession();
		//パラメータの取得
		String performance = request.getParameter("performance");
		String artist = request.getParameter("artist");
		String date = request.getParameter("date");
		String place = request.getParameter("place");
		String number = request.getParameter("number");
		String price = request.getParameter("price");
		String seat = request.getParameter("seat");
		String explanation = request.getParameter("explanation");
		String endDate = request.getParameter("endDate");
		int saleFlag = 0;
		UserDataBeans user = (UserDataBeans)session.getAttribute("userInfo");
		int userId = user.getId();
		String categoryId = request.getParameter("categoryId");
		//userIdを出力
		System.out.println(userId);

		//入力項目に一つでも未入力がある
		if (performance.equals("")||artist.equals("")||date.equals("")||place.equals("")||number.equals("")||price.equals("")||seat.equals("")||explanation.equals("")||endDate.equals("")||user == null||categoryId.equals("")) {
			//スコープにエラ〜メッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/putUp.jsp");
			dispatcher.forward(request,response);
			return;
		}

		//DAOのメソッドを実行
		ItemDAO item = new ItemDAO();
		item.putup(performance, date, artist, place, number, price, seat, explanation, endDate, saleFlag, userId, categoryId);

		//ホーム画面へリダイレクト
		response.sendRedirect("Index");

	}

}
