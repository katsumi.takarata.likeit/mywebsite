package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserDelete
 */
@WebServlet("/UserDelete")
public class UserDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//ログインセッションからユーザーidの取得
		HttpSession session = request.getSession();
		UserDataBeans user = (UserDataBeans)session.getAttribute("userInfo");
		int userId = user.getId();
		// 確認用：idをコンソールに出力
		System.out.println(userId);
		//idを引数にして、idに紐づくユーザ情報を出力する
		UserDAO userdao = new UserDAO();
		UserDataBeans detail = userdao.userDetail(userId);
		//スコープにつめてjspへフォワード
		request.setAttribute("detail", detail);
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userDelete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//ログインセッションからユーザーidの取得
		HttpSession session = request.getSession();
		UserDataBeans user = (UserDataBeans)session.getAttribute("userInfo");
		int userId = user.getId();
		UserDAO userdao = new UserDAO();
		userdao.Delete(userId);
		response.sendRedirect("Index");
	}

}
