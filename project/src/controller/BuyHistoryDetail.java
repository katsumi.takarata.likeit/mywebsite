package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.CategoryMethodDataBeans;
import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.BuyDAO;
import dao.CategoryMethodDAO;
import dao.ItemDAO;

/**
 * Servlet implementation class BuyHistoryDetail
 */
@WebServlet("/BuyHistoryDetail")
public class BuyHistoryDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyHistoryDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//ログインセッションからユーザーidの取得
		HttpSession session = request.getSession();
		UserDataBeans user = (UserDataBeans)session.getAttribute("userInfo");
		int userId = user.getId();

		//Buyに紐付けられたチケットidから購入履歴を出すDAOのメソッドを実行
		String  ticketid = request.getParameter("id");
		int ticketId = Integer.parseInt(ticketid);
		ItemDAO itemDAO = new ItemDAO();
		ItemDataBeans itemDataHistoryDetail = itemDAO.ItemDetail(ticketId);

		//スコープにセット
		request.setAttribute("itemDataHistoryDetail", itemDataHistoryDetail);

		//合計金額を出す,購入枚数を取得
		BuyDAO buyDAO = new BuyDAO();
		BuyDataBeans getTotalPriceTicketBuyNumber = buyDAO.getBuyHistoryDetailByItemId(ticketId);

		//スコープにセット
		request.setAttribute("getTotalPriceTicketBuyNumber", getTotalPriceTicketBuyNumber);

		//カテゴリidを取得
		CategoryMethodDAO categoryMethodDAO = new CategoryMethodDAO();
		int cateid = itemDataHistoryDetail.getCategoryId();

		//カテゴリidを引数にしてカテゴリを取得しスコープにセット
		CategoryMethodDataBeans categoryBeans = categoryMethodDAO.getCategoryMethodDataBeansByID(cateid);
		request.setAttribute("categoryBeans", categoryBeans);

		//jspへフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userBuyHistoryDetail.jsp");
		dispatcher.forward(request, response);
	}

}
