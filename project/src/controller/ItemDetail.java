package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CategoryMethodDataBeans;
import beans.DeliveryMethodDataBeans;
import beans.ItemDataBeans;
import dao.CategoryMethodDAO;
import dao.DeliveryMethodDAO;
import dao.ItemDAO;

/**
 * Servlet implementation class ItemDetail
 */
@WebServlet("/ItemDetail")
public class ItemDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemDetail() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//パラメータを取得
		String itemidd = request.getParameter("id");
		int itemId = Integer.parseInt(itemidd);

		//idを引数にしてDAO実行し、スコープにセット
		ItemDAO itemDAO = new ItemDAO();
		ItemDataBeans itemDetail = itemDAO.ItemDetail(itemId);
		request.setAttribute("itemDetail", itemDetail);

		//配送方法のリストを取得し、スコープにセット
		DeliveryMethodDAO deliveryMethodDAO = new DeliveryMethodDAO();
		ArrayList<DeliveryMethodDataBeans> deliveryMethodList = deliveryMethodDAO.getAllDeliveryMethodDataBeans();
		request.setAttribute("deliveryMethodList", deliveryMethodList);

		//カテゴリidを取得
		CategoryMethodDAO categoryMethodDAO = new CategoryMethodDAO();
		int cateid = itemDetail.getCategoryId();

		//配送Idを引数にして配送方法を取得しスコープにセット
		CategoryMethodDataBeans categoryBeans = categoryMethodDAO.getCategoryMethodDataBeansByID(cateid);
		request.setAttribute("categoryBeans", categoryBeans);

		//jspへフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemDetail.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
