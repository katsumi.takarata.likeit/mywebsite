package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserUpdate
 */
@WebServlet("/UserUpdate")
public class UserUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//ログインセッションからユーザーidの取得
		HttpSession session = request.getSession();
		UserDataBeans user = (UserDataBeans)session.getAttribute("userInfo");
		int userId = user.getId();
		//DAOのメソッドを実行
		UserDAO userDAO = new UserDAO();
		UserDataBeans userDetail = userDAO.userDetail(userId);
		//スコープにセット
		request.setAttribute("userDetail", userDetail);
		//jspへフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//ログインセッションからユーザーidの取得
		HttpSession session = request.getSession();
		UserDataBeans user = (UserDataBeans)session.getAttribute("userInfo");
		int userId = user.getId();
		//リクエストパラメーターを取得
		String password = request.getParameter("password");
		String repassword = request.getParameter("repassword");
		String address = request.getParameter("address");
		String name = request.getParameter("name");
		String birth = request.getParameter("birth");
		//パスワードとパスワード(確認)がどちらも空欄の場合はパスワードは更新せず、パスワード以外の項目を更新する
				if (password.equals("")&&repassword.equals("")) {
					UserDAO userDAO = new UserDAO();
					userDAO.updateNotPass(userId, address, name, birth);
					response.sendRedirect("Index");
					return;
				}

				// passwordとrepasswordの値が異なる時
				if (!password.equals(repassword)) {
//					UserDAO userDAO = new UserDAO();
//					UserDataBeans userDetail = userDAO.userDetail(userId);
//
//					request.setAttribute("userDetail", userDetail);
					// リクエストスコープにエラーメッセージをセット
					request.setAttribute("errMsg", "入力された内容は正しくありません");
					// ログインjspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
					dispatcher.forward(request,response);
					return;
				}

				//入力項目に一つでも未入力
				if (address.equals("")||name.equals("")||birth.equals("")) {
//					UserDAO userDAO = new UserDAO();
//					UserDataBeans userDetail = userDAO.userDetail(userId);
//
//					request.setAttribute("userDetail", userDetail);
					// リクエストスコープにエラーメッセージをセット
					request.setAttribute("errMsg1", "入力された内容は正しくありません");
					// ログインjspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
					dispatcher.forward(request,response);
					return;
				}
		//リクエストパラメーターを引数にしてDAOのメソッドを実行
		UserDAO userDAO = new UserDAO();
		userDAO.update(userId, password, address, name, birth);
		//ホーム画面へリダイレクト
		response.sendRedirect("Index");
	}

}
