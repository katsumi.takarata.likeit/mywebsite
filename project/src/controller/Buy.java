package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CategoryMethodDataBeans;
import beans.DeliveryMethodDataBeans;
import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.BuyDAO;
import dao.CategoryMethodDAO;
import dao.DeliveryMethodDAO;
import dao.ItemDAO;

/**
 * Servlet implementation class Buy
 */
@WebServlet("/Buy")
public class Buy extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Buy() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//パラメータを取得
		String itemidd = request.getParameter("id");
		int itemId = Integer.parseInt(itemidd);

		//選択されたチケットの枚数を取得しスコープにセット
		String numberr = request.getParameter("number");
		int ticketBuyNumber = Integer.parseInt(numberr);
		request.setAttribute("ticketBuyNumber", ticketBuyNumber);

		//idを引数にしてDAO実行し、スコープにセット
		ItemDAO itemDAO = new ItemDAO();
		ItemDataBeans itemDetail = itemDAO.ItemDetail(itemId);
		request.setAttribute("itemDetail", itemDetail);

		//配送idを取得
		String iddd = request.getParameter("delivery_method_id");
		int deliveryId = Integer.parseInt(iddd);

		//配送Idを引数にして配送方法を取得しスコープにセット
		DeliveryMethodDAO deliveryMethodDAO = new DeliveryMethodDAO();
		DeliveryMethodDataBeans derivery = deliveryMethodDAO.getDeliveryMethodDataBeansByID(deliveryId);
		request.setAttribute("derivery", derivery);

		//カテゴリidを取得
		CategoryMethodDAO categoryMethodDAO = new CategoryMethodDAO();
		int cateid = itemDetail.getCategoryId();
		//配送Idを引数にして配送方法を取得しスコープにセット
		CategoryMethodDataBeans categoryBeans = categoryMethodDAO.getCategoryMethodDataBeansByID(cateid);
		request.setAttribute("categoryBeans", categoryBeans);

		//商品の金額と配送料金の合計金額をスコープにセット
		int totalPrice = (itemDetail.getPrice() * ticketBuyNumber) + derivery.getPrice();
		request.setAttribute("totalPrice", totalPrice);

		//jspへフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buy.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//パラメータを取得
				String idd = request.getParameter("id");
				int id = Integer.parseInt(idd);
				//idを引数にしてItemDAOのメソッド実行し、itemフィールドをBuyDAOに入れる
				ItemDAO itemDAO = new ItemDAO();
				ItemDataBeans item = itemDAO.ItemDetail(id);

				//選択されたチケットの枚数を取得
				String numberr = request.getParameter("number");
				int ticketBuyNumber = Integer.parseInt(numberr);

				//配送idを取得
				String iddd = request.getParameter("delivery_method_id");
				int deliveryId = Integer.parseInt(iddd);

				//配送Idを引数にして配送方法を取得
				DeliveryMethodDAO deliveryMethodDAO = new DeliveryMethodDAO();
				DeliveryMethodDataBeans derivery = deliveryMethodDAO.getDeliveryMethodDataBeansByID(deliveryId);

				//セッションスコープからuserIdを取得
				HttpSession session = request.getSession();
				UserDataBeans user = (UserDataBeans)session.getAttribute("userInfo");

				//BuyDAOのメソッドに入れる値を取得
				int userId = user.getId();
				int totalPrice = (item.getPrice() * ticketBuyNumber) + derivery.getPrice();
				int deliveryMethodId = derivery.getId();
				int ticketId = item.getId();

				//値をBuyDAOのメソッドのコンストラクタにセットして実行
				BuyDAO buyDAO = new BuyDAO();
				buyDAO.insertBuy(userId,totalPrice,deliveryMethodId,ticketId,ticketBuyNumber);

				//ticketが売れた確認用にsaleFlagに1を足す ここでitem.saleFlagCountを呼び出していた。宣言を開いたらItemDataBeansだった
				itemDAO.saleFlagCount(ticketId,1);
				//リダイレクト
				response.sendRedirect("Index");
	}

}
