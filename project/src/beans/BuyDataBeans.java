package beans;

import java.util.Date;

import dao.ItemDAO;

public class BuyDataBeans {
	int id;
	int userId;
	int totalPrice;
	int deliveryMethodId;
	int tikcetId;
	Date createDate;
	Date upDate;
	//追加実装
	int ticketBuyNumber;

	//全てをセットするコンストラクタ
	public BuyDataBeans(int idd, int userId2, int totalPrice2, int deliveryMethodId2, int tikcetId2, Date createDate2,
			Date upDate2, int ticketBuyNumber2) {
		this.id = idd;
		this.userId = userId2;
		this.totalPrice = totalPrice2;
		this.deliveryMethodId = deliveryMethodId2;
		this.tikcetId = tikcetId2;
		this.createDate = createDate2;
		this.upDate = upDate2;
		this.ticketBuyNumber = ticketBuyNumber2;
	}
	//アクセサ処理
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}
	public int getDeliveryMethodId() {
		return deliveryMethodId;
	}
	public void setDeliveryMethodId(int deliveryMethodId) {
		this.deliveryMethodId = deliveryMethodId;
	}
	public int getTikcetId() {
		return tikcetId;
	}
	public void setTikcetId(int tikcetId) {
		this.tikcetId = tikcetId;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpDate() {
		return upDate;
	}
	public void setUpDate(Date upDate) {
		this.upDate = upDate;
	}
	public int getTicketBuyNumber() {
		return ticketBuyNumber;
	}
	public void setTicketBuyNumber(int ticketBuyNumber) {
		this.ticketBuyNumber = ticketBuyNumber;
	}

	// 結合データ
	public ItemDataBeans getItemData() {
		ItemDAO itemDAO = new ItemDAO();
		return itemDAO.ItemDetail(this.tikcetId);
	}



}
