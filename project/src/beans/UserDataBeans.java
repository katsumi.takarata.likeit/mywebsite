package beans;

import java.io.Serializable;
import java.util.Date;

public class UserDataBeans implements Serializable {
	private int id;
	private String name;
	private String address;
	private String loginId;
	private String password;
	private Date birthDate;
	private String createDate;
	private String upDate;

	// ログインセッションを保存するためのコンストラクタ
	public UserDataBeans(int id,String name) {
		this.id = id;
		this.name = name;
	}

	//全てをセットするコンストラクタ
	public UserDataBeans(int id, String name, String address, String loginId, Date birthDate, String createDate,
			String upDate) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.loginId = loginId;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.upDate = upDate;
	}








	//アクセサ処理
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpDate() {
		return upDate;
	}
	public void setUpDate(String upDate) {
		this.upDate = upDate;
	}

}
