package beans;

public class CategoryMethodDataBeans {
	int id;
	String name;
	//全てをセットするコンストラクタ
	public CategoryMethodDataBeans(int id2, String name2) {
		this.id = id2;
		this.name = name2;
	}
	public CategoryMethodDataBeans() {
	}
	//アクセサ処理
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}



}
