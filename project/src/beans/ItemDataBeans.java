package beans;

import java.util.Date;

public class ItemDataBeans {
	int id;
	String performance;
	Date date;
	String artist;
	String place;
	int number;
	int price;
	String seat;
	String explanation;
	Date endDate;
	int saleFlag;
	int userId;
	Date createDate;
	Date upDate;
	int categoryId;

	//全てをセットするコンストラクタ
	public ItemDataBeans(int id,String performance,Date date,String artist,String place,int number,int price,String seat,String explanation,Date endDate,int saleFlag,int userId,Date createDate,Date upDate,int categoryId){
		this.id = id;
		this.performance = performance;
		this.date = date;
		this.artist = artist;
		this.place = place;
		this.number = number;
		this.price = price;
		this.seat = seat;
		this.explanation = explanation;
		this.endDate = endDate;
		this.saleFlag = saleFlag;
		this.userId = userId;
		this.createDate = createDate;
		this.upDate = upDate;
		this.categoryId = categoryId;
	}



	//アクセサ処理
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPerformance() {
		return performance;
	}
	public void setPerformance(String performance) {
		this.performance = performance;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	public String getExplanation() {
		return explanation;
	}
	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public int getSaleFlag() {
		return saleFlag;
	}
	public void setSaleFlag(int saleFlag) {
		this.saleFlag = saleFlag;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpDate() {
		return upDate;
	}
	public void setUpDate(Date upDate) {
		this.upDate = upDate;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}







}
