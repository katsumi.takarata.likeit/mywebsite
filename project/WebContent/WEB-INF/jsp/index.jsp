<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--自分のCSS -->
<link rel="stylesheet" href="css/index.css">
<title>チケット売買アプリ ホーム画面</title>
<style type="text/css"></style>
</head>

<body>
	<!-- ナビゲーションバー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!-- サイドバー -->
	<jsp:include page="/baselayout/sidebar.jsp" />
	<!-- ここからメイン -->

				<main>
					<article>
						<h1>For your TICKET</h1>
						<br>
						<image src="image/istockphoto-921957168-1024x1024.jpg"></image>
						<br> <br>
						<h1>最近出品されたチケット</h1>
						<table class="table table-dark">
							<thead>
								<tr>
									<th scope="col">歌手</th>
									<th scope="col">公演名</th>
									<th scope="col">会場</th>
									<th scope="col">価格</th>
									<th scope="col">ページへ飛ぶ</th>
								</tr>
							</thead>
							<tbody>
							<c:forEach var="recently" items="${recentlyItemList}">
									<tr>
										<th scope="row">${recently.artist}</th>
										<td>${recently.performance}</td>
										<td>${recently.place}</td>
										<td>${recently.price}</td>
										<td><a class="btn btn-secondary" href="ItemDetail?id=${recently.id}">Link</a></td>
									</tr>
								</c:forEach>

							</tbody>
						</table>
					</article>
					<br> <br> <br>
					<section>
						<h1>こんなチケットもあるよ</h1>
						<table class="table table-dark">
							<thead>
								<tr>
									<th scope="col">歌手</th>
									<th scope="col">公演名</th>
									<th scope="col">会場</th>
									<th scope="col">価格</th>
									<th scope="col">商品ページへ飛ぶ</th>
								</tr>
							</thead>
							<tbody>
							<c:forEach var="random" items="${randomItemList}">
									<tr>
										<th scope="row">${random.artist}</th>
										<td>${random.performance}</td>
										<td>${random.place}</td>
										<td>${random.price}</td>
										<td><a class="btn btn-secondary" href="ItemDetail?id=${random.id}">Link</a></td>
									</tr>
								</c:forEach>

							</tbody>
						</table>
						<p>チケットあるデー</p>
					</section>

				</main>

				<footer>一番最後に表示させたい</footer>

			</div>
		</div>

	</div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
</body>

</html>
