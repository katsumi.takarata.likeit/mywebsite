<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--自分のCSS -->
<link rel="stylesheet" href="css/index.css">
<title>チケット売買アプリ ホーム出品履歴</title>
<style type="text/css">
<!--
body {

}
-->
</style>
</head>

<body>
	<!-- ナビゲーションバー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!-- サイドバー -->
	<jsp:include page="/baselayout/sidebar.jsp" />

	<!-- ここからメイン -->

	<h1>出品履歴</h1>
	<br>
	<br>
	<table class="table">

			<thead>
				<tr>
					<th scope="col">出品状況</th>
					<th scope="col">出品日</th>
					<th scope="col">公演名</th>
					<th scope="col">歌手</th>
					<th scope="col">価格</th>
					<th scope="col">詳細へ</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="putUpHistory" items="${putUpHistoryList}">
				<tr><th scope="col">
				<c:if test="${putUpHistory.saleFlag == 0}">出品中
				<a class="btn btn-primary"
						href="PutUpUpdate?id=${putUpHistory.id}">編集</a>
						<a class="btn btn-danger"
						href="PutUpDelete?id=${putUpHistory.id}">取り消し</a></c:if>
						<c:if test="${putUpHistory.saleFlag == 1}">取引終了</c:if></th>
					<th scope="row">${putUpHistory.createDate}</th>
					<td>${putUpHistory.performance}</td>
					<td>${putUpHistory.artist}</td>
					<td>${putUpHistory.price}</td>
					<td><a class="btn btn-success"
						href="PutUpHistoryDetail?id=${putUpHistory.id}">Link</a></td>
				</tr>
		</c:forEach>
		</tbody>
	</table>

	<footer>一番最後に表示させたい</footer>

	</div>
	</div>

	</div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
</body>

</html>
