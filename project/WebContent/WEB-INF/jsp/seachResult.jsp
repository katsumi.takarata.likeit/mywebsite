<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!--自分のCSS -->
<link rel="stylesheet" href="css/index.css">
<title>チケット売買アプリ 検索結果</title>
<style type="text/css">
<!--
body {

}
-->
</style>
</head>

<body>
	<!-- ナビゲーションバー -->
	<jsp:include page="/baselayout/header.jsp" />

	<!-- サイドバー -->
	<jsp:include page="/baselayout/sidebar.jsp" />
	<!-- ここからメイン -->

				<h1>検索結果</h1>
				<br> <br>
				<table class="table">
					<thead>
						<tr>
							<th scope="col">開催日</th>
							<th scope="col">公演名</th>
							<th scope="col">歌手</th>
							<th scope="col">価格</th>
							<th scope="col">枚数</th>
							<th scope="col">出品終了日</th>
							<th scope="col">詳細へ</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="seachResult" items="${seachResultList}">
							<tr>
								<th scope="row">${seachResult.date}</th>
								<td>${seachResult.performance}</td>
							    <td>${seachResult.artist}</td>
								<td>${seachResult.price}</td>
								<td>${seachResult.number}</td>
								<td>${seachResult.endDate}</td>
								<td><a class="btn btn-secondary"
									href="ItemDetail?id=${seachResult.id}">Link</a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>

				<footer>一番最後に表示させたい</footer>

			</div>
		</div>

	</div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
</body>

</html>
