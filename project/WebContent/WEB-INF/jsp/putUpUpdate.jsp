<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!--自分のCSS -->
    <link rel="stylesheet" href="css/index.css">

    <title>チケット売買アプリ 出品編集</title>
    <style type="text/css">

    </style>
</head>

<body>
    <!-- ナビゲーションバー -->
     <jsp:include page="/baselayout/header.jsp" />

     <!-- サイドバー -->
	<jsp:include page="/baselayout/sidebar.jsp" />

    <!-- ここからメイン -->


	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
	<c:if test="${errMsg1 != null}">
		<div class="alert alert-danger" role="alert">${errMsg1}</div>
	</c:if>

	<h1 class="text-center">出品編集</h1>
	<div class="container">



		<form action="PutUpUpdate" method="post">
		<input type="hidden" name="itemId" value="${item.id}">
<!--
			<div class="form-group row">
				<label for="id" class="col-sm-2 col-form-label">商品画像</label>
				<div class="col-sm-10">
                    <input type="file" multiple />

				</div>
			</div>
-->
			<h3 align=center>商品ID ${item.id}</h3>
			<br><br>
			<div class="form-group row">
				<label for="performance" class="col-sm-2 col-form-label">公演名</label>
				<div class="col-sm-10">
					<input type="text" name="performance" class="form-control"
						id="performance"  value="${item.performance}">
				</div>
			</div>
            <div class="form-group row">
				<label for="artist" class="col-sm-2 col-form-label">歌手</label>
				<div class="col-sm-10">
					<input type="text" name="artist" class="form-control"
						id="artist" placeholder="${item.artist}" value="${item.artist}">
				</div>
			</div>
			<div class="form-group row">
				<label for="date" class="col-sm-2 col-form-label">開催日</label>
				<div class="col-sm-10">
					<input type="date" name="date" class="form-control" id="date" placeholder="${item.date}" value="${item.date}">
				</div>
            </div>
            <div class="form-group row">
				<label for="place" class="col-sm-2 col-form-label">会場</label>
				<div class="col-sm-10">
					<input type="text" name="place" class="form-control" id="place" placeholder="${item.place}" value="${item.place}">
				</div>
			</div>
			<div class="form-group row">
				<label for="number" class="col-sm-2 col-form-label">枚数</label>
				<div class="col-sm-10">
					<input type="text" name="number" class="form-control" id="number" placeholder="${item.number}" value="${item.number}">
				</div>
			</div>
			<div class="form-group row">
				<label for="price" class="col-sm-2 col-form-label">一枚あたりの値段</label>
				<div class="col-sm-10">
					<input type="text" name="price" class="form-control" id="price" placeholder="${item.price}" value="${item.price}">
				</div>
			</div>
            <div class="form-group row">
				<label for="seat" class="col-sm-2 col-form-label">席</label>
				<div class="col-sm-10">
					<input type="text" name="seat" class="form-control" id="seat" placeholder="${item.seat}" value="${item.seat}">
				</div>
			</div>
			<div class="form-group row">
				<label for="category" class="col-sm-2 col-form-label">カテゴリ</label>
				<div class="col-sm-10">
				<select name="categoryId">
								<c:forEach var="category" items="${categoryList}">
								<option value="${category.id}">${category.name}</option>
								</c:forEach>
							</select>
				</div>
			</div>
            <div class="form-group row">
				<label for="explanation" class="col-sm-2 col-form-label">出品者からの説明</label>
				<div class="col-sm-10">
					<input type="text" name="explanation" class="form-control" id="explanation" placeholder="${item.explanation}" value="${item.explanation}">
				</div>
			</div>
            <div class="form-group row">
				<label for="endDate" class="col-sm-2 col-form-label">出品終了日</label>
				<div class="col-sm-10">
					<input type="date" name="endDate" class="form-control" id="endDate" placeholder="${item.endDate}" value="${item.endDate}">
				</div>
			</div>
			<div class="submitButton">
				<div class="form-group row">
					<div class="col-sm-12">
						<button type="submit" class="btn btn-secondary btn-lg">更新</button>
					</div>
				</div>
			</div>

		</form>

	</div>
    </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body></html>

