<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!--自分のCSS -->
    <link rel="stylesheet" href="css/index.css">

    <title>チケット売買アプリ 退会画面</title>
    <style type="text/css">

    </style>
</head>

<body>
    <!-- ナビゲーションバー -->
    <jsp:include page="/baselayout/header.jsp" />

    <!-- サイドバー -->
	<jsp:include page="/baselayout/sidebar.jsp" />

    <!-- ここからメイン -->

    <h1 class="text-center">ユーザー情報削除</h1>

		<div class="delete-area">
			<p align="center">ID${detail.id}を消去しますか？</p>
			<div class="row">
				<div class="col-sm-6" align="center">
					<a href="Index" class="btn btn-primary">いいえ</a>
				</div>
				<div class="col-sm-6" align="center">
					<form action="UserDelete" method="post">
						<input type="hidden" name="id" value="${detail.id}">
						<button type="submit" class="btn btn-danger">はい</button>
					</form>
				</div>
			</div>
		</div>
	</div>
        </div>
    </div>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
