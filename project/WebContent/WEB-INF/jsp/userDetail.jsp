<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!--自分のCSS -->
    <link rel="stylesheet" href="css/index.css">

    <title>チケット売買アプリ ユーザー詳細</title>
    <style type="text/css">

    </style>
</head>

<body>
    <!-- ナビゲーションバー -->
    <jsp:include page="/baselayout/header.jsp" />

    <!-- サイドバー -->
	<jsp:include page="/baselayout/sidebar.jsp" />

    <!-- ここからメイン -->

        <h1 class="text-center">ユーザー情報詳細参照</h1>
        <br>
        <br>
        <div class="form-group row">
            <label for="ID" class="col-sm-6 col-form-label" align="center">ログインID</label>
            <div class="col-sm-6">
                <p class="form-control-plaintext">${detail.loginId}</p>
            </div>
        </div>
        <div class="form-group row">
            <label for="name" class="col-sm-6 col-form-label" align="center">ユーザー名</label>
            <div class="col-sm-6">
                <p class="form-control-plaintext">${detail.name}</p>
            </div>
        </div>
        <div class="form-group row">
            <label for="birth" class="col-sm-6 col-form-label" align="center">生年月日</label>
            <div class="col-sm-6">
                <input type="text" readonly class="form-control-plaintext" id="birth" value="${detail.birthDate}">
            </div>
        </div>
        <div class="form-group row">
            <label for="address" class="col-sm-6 col-form-label" align="center">住所</label>
            <div class="col-sm-6">
                <input type="text" readonly class="form-control-plaintext" id="address" value="${detail.address}">
            </div>
        </div>
        <div class="form-group row">
            <label for="createdate" class="col-sm-6 col-form-label" align="center">登録日時</label>
            <div class="col-sm-6">
                <input type="text" readonly class="form-control-plaintext" id="createdated" value="${detail.createDate}">
            </div>
        </div>
        <div class="form-group row">
            <label for="update" class="col-sm-6 col-form-label" align="center">更新日時</label>
            <div class="col-sm-6">
                <input type="text" readonly class="form-control-plaintext" id="update" value="${detail.upDate}">
            </div>
        </div>
            </div>
        </div>


        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>