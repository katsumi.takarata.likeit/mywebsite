<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!--自分のCSS -->
    <link rel="stylesheet" href="css/index.css">
    <title>チケット売買アプリ 出品履歴詳細</title>
    <style type="text/css">
        <!--
        body {}
        -->
    </style>
</head>

<body>
    <!-- ナビゲーションバー -->
     <jsp:include page="/baselayout/header.jsp" />

     <!-- サイドバー -->
	<jsp:include page="/baselayout/sidebar.jsp" />
    <!-- ここからメイン -->

                <h1>出品履歴詳細</h1>
                <br>
                <br>

                <table class="table">

                    <tbody>
                    	<tr>
                            <th scope="row" align="center">公演名</th>
                            <td>${itemDetail.performance}</td>
                        </tr>                        <tr>
                            <th scope="row" align="center">公演名</th>
                            <td>${itemDetail.performance}</td>
                        </tr>
                        <tr>
                            <th scope="row" align="center">歌手</th>
                            <td>${itemDetail.artist}</td>
                        </tr>
                        <tr>
                            <th scope="row" align="center">会場</th>
                            <td>${itemDetail.place}</td>
                        </tr>
                        <tr>
                            <th scope="row">開催日</th>
                            <td>${itemDetail.date}</td>
                        </tr>
                        <tr>
                            <th scope="row">枚数</th>
                            <td><c:if test="${ticketBuyNumber != null}" >${ticketBuyNumber}</c:if><c:if test="${itemNumber != null}" >出品中  ${itemNumber}</c:if></td>
                        </tr>
                        <tr>
                            <th scope="row">一枚当たりの価格</th>
                            <td>${itemDetail.price}</td>
                        </tr>
                        <tr>
                            <th scope="row">席</th>
                            <td>${itemDetail.seat}</td>
                        </tr>
                        <tr>
                            <th scope="row">カテゴリ</th>
                            <td>${categoryBeans.name}</td>
                        </tr>
                       <tr>
                            <th scope="row">出品者からの説明</th>
                            <td>${itemDetail.explanation}</td>
                        </tr>
                        <tr>
                            <th scope="row">出品終了日</th>
                            <td>${itemDetail.endDate}</td>
                        </tr>
                  </tbody>
                </table>

                <footer>一番最後に表示させたい</footer>

            </div>
        </div>

    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body></html>
