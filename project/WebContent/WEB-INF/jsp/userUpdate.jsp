<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!--自分のCSS -->
    <link rel="stylesheet" href="css/index.css">

    <title>チケット売買アプリ ユーザー情報更新</title>
    <style type="text/css">

    </style>
</head>

<body>
    <!-- ナビゲーションバー -->
    <jsp:include page="/baselayout/header.jsp" />

    <!-- サイドバー -->
	<jsp:include page="/baselayout/sidebar.jsp" />

    <!-- ここからメイン -->
    <h1 class="text-center">ユーザー情報更新</h1>

<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
	<c:if test="${errMsg1 != null}">
		<div class="alert alert-danger" role="alert">${errMsg1}</div>
	</c:if>





		<form action="UserUpdate" method="post">
			<input type="hidden" name="id" value="${userDetail.id}">
			<div class="form-group row">
				<label for="id" class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-10">${userDetail.loginId}</div>
			</div>
			<div class="form-group row">
				<label for="password" class="col-sm-2 col-form-label">パスワード</label>
				<div class="col-sm-10">
					<input type="password" name="password" class="form-control"
						id="password" placeholder="password">
				</div>
			</div>
			<div class="form-group row">
				<label for="repassword" class="col-sm-2 col-form-label">パスワード（確認）</label>
				<div class="col-sm-10">
					<input type="password" name="repassword" class="form-control"
						id="repassword" placeholder="password">
				</div>
			</div>
			<div class="form-group row">
				<label for="name" class="col-sm-2 col-form-label">住所</label>
				<div class="col-sm-10">
					<input type="text" name="address" class="form-control" id="name"
						placeholder="住所" value="${userDetail.address}">
				</div>
			</div>
			<div class="form-group row">
				<label for="name" class="col-sm-2 col-form-label">ユーザー名</label>
				<div class="col-sm-10">
					<input type="text" name="name" class="form-control" id="name"
						placeholder="name" value="${userDetail.name}">
				</div>
			</div>
			<div class="form-group row">
				<label for="birth" class="col-sm-2 col-form-label">生年月日</label>
				<div class="col-sm-10">
					<input type="date" name="birth" class="form-control"
						value="${userDetail.birthDate}" id="birth">
				</div>
			</div>
			<div class="submitButton">
				<div class="form-group row">
					<div class="col-sm-12">
						<button type="submit" class="btn btn-secondary btn-lg" name="update">更新</button>
					</div>
				</div>
			</div>

		</form>

	</div>
    </div>
        </div>
    </div>


        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>