<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
    <header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="Index">チケット売買アプリ</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <form action="SeachResult" method="Post" class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2" type="search" name="seachword" placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">検索</button>
                        </form>
                        <li class="nav-item active">
                            <a class="nav-link" href="Index"> ホーム <span class="sr-only">(current)</span></a>
                        </li>
                        <c:if test="${userInfo != null}">
                        <li class="nav-item">
                            <a class="nav-link" href="#">${userInfo.name} さん</a>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                マイページ
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="UserDetail">ユーザー詳細情報</a>
                                <a class="dropdown-item" href="UserUpdate">ユーザー情報更新</a>
                                <a class="dropdown-item" href="UserDelete">ユーザー情報削除</a>
                                <a class="dropdown-item" href="BuyHistory">購入履歴</a>
                                <a class="dropdown-item" href="PutUpHistory">出品履歴</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="LogOut">ログアウト</a>
                            </div>
                        </li>
                        </c:if>
                        <li class="nav-item">
                            <a class="nav-link" href="Login">ログイン</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="Create">新規登録</a>
                        </li>
                        <c:if test="${userInfo != null}">
                        <li class="nav-item">
                            <a class="nav-link" href="PutUp">出品</a>
                        </li>

                        </c:if>
                    </ul>
                </div>
            </div>
        </nav>
    </header>